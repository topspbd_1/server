package ro.alexi.awbd.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import ro.alexi.awbd.dtos.AddressDTO;
import ro.alexi.awbd.dtos.CustomUserDetails;
import ro.alexi.awbd.dtos.paginationDTO.AddressPaginationResponse;
import ro.alexi.awbd.mapper.AddressMapper;
import ro.alexi.awbd.mapper.AddressMapperImpl;
import ro.alexi.awbd.model.Address;
import ro.alexi.awbd.model.Customer;
import ro.alexi.awbd.repository.AddressRepository;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AddressMapperImpl.class})
class AddressServiceTest {

    AddressService addressService;

    @Autowired
    private AddressMapper addressMapper;

    @Mock
    AddressRepository addressRepository;


    @Mock
    private SecurityContext securityContext;


    @BeforeEach
    void setUp() {
        Authentication mockAuth = Mockito.mock(Authentication.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(mockAuth);
        when(mockAuth.getPrincipal()).thenReturn(customUserDetails());
        SecurityContextHolder.setContext(securityContext);

        addressService = new AddressService(addressRepository, addressMapper);
    }


    @Test
    void getAddressesByCustomer() {
        Pageable pageable = PageRequest.of(0, 10);

        when(addressRepository.findAllByCustomerId(any(), any())).thenReturn(pageAddress(pageable));
        AddressPaginationResponse addressesByCustomer = addressService.getAddressesByCustomer(pageable);

        assertEquals(addressesByCustomer.getAddresses().size(), 1);
        assertEquals(addressesByCustomer.getTotalItems(), 1);
    }

    @Test
    void createAddress() {
        when(addressRepository.save(any())).thenReturn(address());

        AddressDTO savedAddress = addressService.createAddress(addressDTO());

        assertEquals(savedAddress.getAddress(), "TEST_ADDRESS");
        assertEquals(savedAddress.getCity(), "TEST_CITY");
        assertEquals(savedAddress.getPostalCode(), "TEST_POSTALCODE");
    }

    private Page<Address> pageAddress(Pageable pageable) {
        Address address = new Address();
        address.setAddress("TEST_ADDRESS");
        address.setCity("TEST_CITY");
        address.setPostalCode("TEST_POSTAL_CODE");
        List<Address> addresses = Collections.singletonList(address);
        return new PageImpl<>(addresses, pageable, addresses.size());
    }

    private CustomUserDetails customUserDetails() {
        Customer customer = new Customer();
        customer.setEmail("TEST");
        return new CustomUserDetails("TEST", "TEST", Collections.emptyList(), UUID.randomUUID(), customer);
    }

    private AddressDTO addressDTO() {
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setAddress("TEST_ADDRESS");
        addressDTO.setCity("TEST_CITY");
        addressDTO.setPostalCode("TEST_POSTALCODE");

        return addressDTO;
    }

    private Address address() {
        Address address = new Address();
        address.setAddress("TEST_ADDRESS");
        address.setCity("TEST_CITY");
        address.setPostalCode("TEST_POSTALCODE");

        return address;
    }
}
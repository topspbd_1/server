package ro.alexi.awbd.service;

import com.fasterxml.jackson.databind.ser.std.UUIDSerializer;
import org.h2.value.ValueUuid;
import org.hibernate.id.UUIDGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import ro.alexi.awbd.config.ConfigClass;
import ro.alexi.awbd.dtos.AddressDTO;
import ro.alexi.awbd.dtos.CustomUserDetails;
import ro.alexi.awbd.dtos.OrderDTO;
import ro.alexi.awbd.dtos.ProductDTO;
import ro.alexi.awbd.dtos.paginationDTO.AddressPaginationResponse;
import ro.alexi.awbd.dtos.paginationDTO.OrderPaginationResponse;
import ro.alexi.awbd.jwt.JwtTokenUtil;
import ro.alexi.awbd.mapper.CustomerMapper;
import ro.alexi.awbd.mapper.OrderMapper;
import ro.alexi.awbd.model.Address;
import ro.alexi.awbd.model.Customer;
import ro.alexi.awbd.model.Order;
import ro.alexi.awbd.repository.CustomerRepository;
import ro.alexi.awbd.repository.OrderRepository;
import ro.alexi.awbd.security.SecurityUserService;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {OrderMapperImpl.class})
public class OrderServiceTest {
    OrderService orderService;

    @Mock
    OrderRepository orderRepository;

    @Autowired
    OrderMapper orderMapper;

    @Mock
    private SecurityContext securityContext;

    @BeforeEach
    void setUp() {
        Authentication mockAuth = Mockito.mock(Authentication.class);
        Mockito.when(securityContext.getAuthentication()).thenReturn(mockAuth);
        when(mockAuth.getPrincipal()).thenReturn(customUserDetails());
        SecurityContextHolder.setContext(securityContext);

        orderService = new OrderService(orderRepository, orderMapper);
    }

    @Test
    void getOrders() {
        Pageable pageable = PageRequest.of(0, 10);

        when(orderRepository.findAll(any())).thenReturn(pageOrder(pageable));
        OrderPaginationResponse orders = orderService.getOrders(pageable);

        assertEquals(orders.getOrders().size(), 1);
        assertEquals(orders.getTotalItems(), 1);
    }

    @Test
    void placeOrder() {
        when(orderRepository.save(any())).thenReturn(order());

        OrderDTO placedOrder = orderService.placeOrder(orderDTO());

        assertEquals(placedOrder.getOrderDetails(), "TEST_Details");
        assertEquals(placedOrder.getDeliveryAddress(), "TEST_ADDRESS");
    }

    private Page<Order> pageOrder(Pageable pageable) {
        Order order = new Order();
        order.setId(UUID.FromString("44e128a5-ac7a-4c9a-be4c-224b6bf81b20"));
        order.setOrderDetails("TEST_Details");
        order.setOrderDate(ZonedDateTime.now());
        order.setOrderNumber(123L);
        order.setDeliveryAddress("TEST_ADDRESS");
        order.setProducts(Collections.emptyList());
        List<Order> orders = Collections.singletonList(order);
        return new PageImpl<Order>(orders, pageable, orders.size());
    }

    private CustomUserDetails customUserDetails() {
        Customer customer = new Customer();
        customer.setEmail("TEST");
        return new CustomUserDetails("TEST", "TEST", Collections.emptyList(), UUID.randomUUID(), customer);
    }

    private OrderDTO orderDTO() {
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setId(UUID.FromString("44e128a5-ac7a-4c9a-be4c-224b6bf81b20"));
        orderDTO.setOrderDetails("TEST_Details");
        orderDTO.setOrderDate(ZonedDateTime.now());
        orderDTO.setOrderNumber(123L);
        orderDTO.setDeliveryAddress("TEST_ADDRESS");
        orderDTO.setProducts(Collections.emptyList());

        return orderDTO;
    }

    private Order order() {
        Order order = new Order();
        order.setId(UUID.FromString("44e128a5-ac7a-4c9a-be4c-224b6bf81b20"));
        order.setOrderDetails("TEST_Details");
        order.setOrderDate(ZonedDateTime.now());
        order.setOrderNumber(123L);
        order.setDeliveryAddress("TEST_ADDRESS");
        order.setProducts(Collections.emptyList());

        return order;
    }
}

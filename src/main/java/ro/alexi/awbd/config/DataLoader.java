package ro.alexi.awbd.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import ro.alexi.awbd.dtos.enums.AuthorityEnum;
import ro.alexi.awbd.model.Customer;
import ro.alexi.awbd.repository.CustomerRepository;

import java.util.UUID;

@Component
public class DataLoader implements ApplicationRunner {

    private final CustomerRepository customerRepository;
    private final ConfigClass configClass;

    @Autowired
    public DataLoader(CustomerRepository customerRepository, ConfigClass configClass) {
        this.customerRepository = customerRepository;
        this.configClass = configClass;
    }

    public void run(ApplicationArguments args) {

        if (customerRepository.findByAuthority(AuthorityEnum.ADMIN.getValue()).isEmpty()) {
            Customer admin = new Customer();
            admin.setEmail("admin@admin.com");
            admin.setAuthority(AuthorityEnum.ADMIN.getValue());
            admin.setId(UUID.randomUUID());
            admin.setName("admin");
            admin.setPhoneNumber("+4077772727");
            admin.setPassword(configClass.passwordEncoder().encode("admin"));
            customerRepository.save(admin);
        }
    }
}
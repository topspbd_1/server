package ro.alexi.awbd.dtos.paginationDTO;


import ro.alexi.awbd.dtos.SupplierDTO;

import java.util.List;


public class SupplierPaginationResponse {
    private List<SupplierDTO> suppliers;
    private long totalItems;


    public List<SupplierDTO> getSuppliers() {
        return suppliers;
    }

    public void setSuppliers(List<SupplierDTO> suppliers) {
        this.suppliers = suppliers;
    }

    public long getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(long totalItems) {
        this.totalItems = totalItems;
    }
}

package ro.alexi.awbd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ro.alexi.awbd.model.Supplier;

@SpringBootApplication
public class AwbdApplication {

    public static void main(String[] args) {
        SpringApplication.run(AwbdApplication.class, args);
    }

}

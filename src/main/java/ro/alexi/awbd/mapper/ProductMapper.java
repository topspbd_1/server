package ro.alexi.awbd.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ro.alexi.awbd.dtos.ProductDTO;
import ro.alexi.awbd.model.Product;

@Mapper(componentModel = "spring", uses = {})
public interface ProductMapper extends EntityMapper<ProductDTO, Product> {

    @Mapping(target = "id", source = "id")
    Product toEntity(ProductDTO productDTO);

    @Mapping(target = "id", source = "id")
    ProductDTO toDto(Product product);
}
